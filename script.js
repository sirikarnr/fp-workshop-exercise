document.getElementById('fileinput').addEventListener('change', readFileFromEvent, false)

function readFileFromEvent (event) {
	var file = event.target.files[0]
	var r = new FileReader()
	r.onload = updateContent
	r.readAsBinaryString(file)
}

function logFileLoad(event) {
	var content = event.target.result
	console.log(content)
}

function updateContent(event) {
	// logFileLoad(event)
	setContent(parseMarkDown(event.target.result))
}

function setContent(content) {
	document.getElementById("main").innerHTML = content
}

function parseMarkDown (content) {	
    //complete content with <h> <p> and <b> with only the *word* format 
	var htmlContent = input => toStringNoSpace(R.map(processHtmlTag,split2Line(input)))
	
	//split each paragraph to replace with <b>
	var addNewLineToTagP = inputWithHtml => inputWithHtml.replace(/<p>/gi,"\n<p>")
	var seperateLineTagP = R.compose(splitLine,addNewLineToTagP)
	var addBoldArr = paragraph => seperateLineTagP(htmlContent(paragraph))

	//var addBoldArr = x => splitLine(htmlWithBold(x).replace(/<p>/gi,"\n<p>"))

	//replace paragraph with <b> for specific format *word1 word2*
	var completeContent = content => toStringNoSpace(R.map(processBold,addBoldArr(content)))
	return completeContent(content)
}
        //########## Add HTML Tag to Content ##################
		var processHtmlTag = content => {
			if(countSharp(content) > 0){
				return addTagH(countSharp(content))(dropToString(isSharp,content))
			}else{
				return addTagP(processBold(content))
			}
		}

		//######### Check and Replace Bold Character ########
		var matchBold = content => R.match(/\*(.*?)\*|(\*).(.*?).(\*)/,content)
		var processBold = paragraph => {
			if(matchBold(paragraph).length > 0){
				var starWord = dropToString(isNotStar,paragraph)
				var noFirstStar = dropToString(isStar,starWord)
				var boldWord = takeToString(isNotStar,noFirstStar)
				return paragraph.replace("*"+boldWord+"*",addTagB(boldWord))
			}else{
				return paragraph
			}
		}
		//###### Split Curry #######		
		var split2Line = R.split('\n\n')
		var splitSpace = R.split(' ')
		var splitLine = R.split('\n')

		//######### Array To String ############
		var toStringNoSpace = content => R.trim(R.join('',content))

		//######### Count Special Character ############
		var countSharp = content => R.takeWhile(isSharp,content).length

		//######### Add HTML Tag ###############
		var addHtml = tag => content => "<" + tag + ">" + content + "</" + tag + ">"
		var addHtmlCurry = R.curry(addHtml)
		var addTagP = addHtmlCurry("p")
		var addTagB = addHtmlCurry("b")
		// var addTagH = addHtmlCurry("h")

		var addTagH =  level => content => "<h" + level + ">" + content + "</h" + level + ">"

		//######### Special Character Condition ##########
		var isSharp = paragraph => R.equals(paragraph,'#')
		var isStar = paragraph => R.equals(paragraph,'*')
		var isNotStar = paragraph => !R.equals(paragraph,'*')
		//######### Remove Special Character ############
		var dropCond = (condition,str) => R.dropWhile(condition,str)
		var takeCond = (condition,str) => R.takeWhile(condition,str)
		 var takeToString = R.compose(toStringNoSpace,takeCond)
		 var dropToString = R.compose(toStringNoSpace,dropCond)

		// var takeToString = R.compose(toStringNoSpace,takeOrDropWhile("take"))
		// var dropToString = R.compose(toStringNoSpace,takeOrDropWhile("drop"))

		// function takeOrDropWhile(action){
		// 	if(action=="take") return (condition,str) => R.takeWhile(condition,str)
		// 	else return (condition,str) => R.dropWhile(condition,str)
		// }
